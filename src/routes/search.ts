import { NextFunction, Request, Response, Router } from "express";
import { BaseRoute } from "./route";
import logger from "../utils/logger";

/**
 * /search route
 *
 * @class SearchRoute
 */
export class SearchRoute extends BaseRoute {

    /**
     * Create the route for the search page.
     *
     * @class SearchRoute
     * @method create
     * @param router {Router} an instanced Express Router on which to hook the class' routes
     * @static
     */
    public static create (router: Router) {
        // log to the console that the route was created
        logger.debug("[SearchRoute::create] Creating the search page route.");

        // add the search route to the application
        router.get("/search", (req: Request, res: Response, next: NextFunction) => {
            new SearchRoute().get(req, res, next);
        });
    }

    /**
     * Constructor
     *
     * @class SearchRoute
     * @constructor
     */
    constructor () {
        super();
    }


    /**
     * The get method for the search page route.
     *
     * Actually searching for something should return some results.
     * We should not decide on what category of search - do all three and then filter them in the results page.
     * Later we'd build in advanced querying like type:character or issue:44 so that it's up to the user if they want to narrow their query.
     * These filters would also be available as quick options in a left pane (think Seek or Indeed style filtering)
     *
     * @class SearchRoute
     * @method get
     * @param req {Request} The Express Request object
     * @param res {Response} The Express Response object
     * @param next {NextFunction} The next method to execute.
     */
    public get (req: Request, res: Response, next: NextFunction) {
        // set the page title
        this.title = "Search | _query_ | Marvel Comics Explorer";

        // set options
        let options: Object = {};

        // render the relevant template
        this.render(req, res, "search", options);
    }
}
