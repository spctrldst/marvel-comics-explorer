import { NextFunction, Request, Response } from "express";

/**
 * Constructor
 *
 * @class BaseRoute
 */
export class BaseRoute {
    protected title: string;
    private scripts: string[];

    /**
     * Constructor
     *
     * @class BaseRoute
     * @constructor
     */
    constructor () {
        // initialize variables
        this.title = "Marvel Comics Explorer";
        this.scripts = [];
    }

    /**
     * Add a JS external file to the request.
     *
     * @class BaseRoute
     * @method addScript
     * @param src {String} The src of the external JS file.
     * @return {BaseRoute} Self for chaining later.
     */
    public addScript (src: string): BaseRoute {
        this.scripts.push(src);
        return this;
    }

    /**
     * Render the requested page.
     *
     * The template should be a fully functional PugJS template. It can have native imports and require external files.
     * It should have its own CSS but can share global CSS variables through the use of postcss-cssnext imports.
     *
     * @class BaseRoute
     * @method render
     * @param req {Request} The request object from Express.
     * @param res {Response} The response object from Express.
     * @param view {String} The view to render - a pugJS template file that gets processed.
     * @param options {Object} Additional options that are appended to the view's local scope.
     * @return void
     */
    public render(req: Request, res: Response, view: string, options?: Object) {
        // add constants
        res.locals.BASE_URL = "/";

        // add scripts
        res.locals.scripts = this.scripts;

        // add title
        res.locals.title = this.title;

        // render the view
        res.render(view, options);
    }
}
