import { NextFunction, Request, Response, Router } from "express";
import { BaseRoute } from "./route";
import logger from "../utils/logger";

/**
 * /result route
 *
 * @class ResultRoute
 */
export class ResultRoute extends BaseRoute {
    /**
     * Create the routes.
     *
     * @class ResultRoute
     * @method create
     * @param router {Router} an instanced Express Router on which to hook the class' routes
     * @static
     */
    public static create (router: Router) {
        // log to console
        logger.debug("[ResultRoute::create] Creating the index route.");

        // add the home page route to the application
        router.get("/result", (req: Request, res: Response, next: NextFunction) => {
            new ResultRoute().get(req, res, next);
        });
    }
    /**
     * Constructor
     *
     * @class ResultRoute
     * @constructor
     */
    constructor () {
        super();
    }

    /**
     * The ResultPage Get route.
     *
     * This method will fall back to the template to decide whether to render an issue, a character, or canon info.
     * It's done this way because I feel that it should be like Wikipedia in that every page is the same (but may include different elements
     * depending on the object's type.
     *
     * @class ResultRoute
     * @method get
     * @param req {Request} The express Request object.
     * @param res {Response} The express Response object.
     * @param next {NextFunction} The next method to execute.
     */
    public get (req: Request, res: Response, next: NextFunction) {
        // set a custom title based on the route, in this case home
        this.title = "result_type | result_name | Marvel Comics Explorer";

        // set options
        let options: Object = {};

        // render the template for this view
        this.render(req, res, "result", options);
    }
}
