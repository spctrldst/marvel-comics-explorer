import { NextFunction, Request, Response, Router } from "express";
import { BaseRoute } from "./route";
import logger from "../utils/logger";

/**
 * / route
 *
 * @class IndexRoute
 */
export class IndexRoute extends BaseRoute {
    /**
     * Create the routes for this class.
     *
     * We won't have any method other than 'index' for this class because we're not really doing anything with it other than loading the application.
     * If we were to build in specific browser versions (i.e. IE users see a different version) then I'd add in more advanced methods for that.
     * Most data queries would be handled by API routes later in development.
     *
     * @class IndexRoute
     * @method create
     * @param router {Router} an instanced Express Router on which to hook the class' routes
     * @static
     */
    public static create (router: Router) {
        // log to console that we created the route
        logger.debug("[IndexRoute::create] Creating the index route.");

        // add the home page route to the application (we can specify more methods here later)
        router.get("/", (req: Request, res: Response, next: NextFunction) => {
            new IndexRoute().index(req, res, next);
        });
    }
    /**
     * Constructor
     *
     * @class IndexRoute
     * @constructor
     */
    constructor () {
        super();
    }

    /**
     * The home page route.
     *
     * The user gets a nice big 'Marvel' splash screen with an autofocus search box and a little bit of attribution at the bottom of the page.
     * In the future it'll pull in comic images and have a slowly-scrolling background to give the page a little bit of life and colour.
     *
     * @class IndexRoute
     * @method index
     * @param req {Request} The express Request object.
     * @param res {Response} The express Response object.
     * @param next {NextFunction} The next method to execute.
     */
    public index (req: Request, res: Response, next: NextFunction) {
        // set a custom title based on the route, in this case home (eventually the application's title would be templated)
        this.title = "Begin | Marvel Comics Explorer";

        // set options for the home page - just core attributes for now
        // might include an option later for pre-fetching query results or pre-filling the search box
        let options: Object = {
            "headerText": "marvel",
            "subheaderText": "comics",
            "attributionStatement": "content © Marvel Comics 2017 - all rights reserved"
        };

        // render the template for this view
        this.render(req, res, "index", options);
    }
}
