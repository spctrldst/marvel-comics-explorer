import * as bodyParser from "body-parser";
import * as cookieParser from "cookie-parser";
import * as express from "express";
import * as morgan from "morgan";
import * as path from "path";
import errorHandler = require("errorhandler");
import methodOverride = require("method-override");

import logger from "./utils/logger";

import { IndexRoute } from "./routes/index";
import { SearchRoute } from "./routes/search";
import { ResultRoute } from "./routes/result";

/**
 * The server.
 *
 * The Server is the core of the application. The bootstrap method creates the server and returns it.
 *
 * @class Server
 */

export class Server {

    public app: express.Application;

    /**
     * Bootstrap the application.
     *
     * @class Server
     * @method bootstrap
     * @static
     * @return {ng.auto.IInjectorService} Returns the newly created injector for the application.
     */
    public static bootstrap (): Server {
        return new Server();
    }

    /**
     * Constructor function.
     *
     * We set the app property to a new instance of an express application. We call the init functions (config, routes, an API).
     * Each of these functions are called in sequence - thus if order of execution is a requirement they should be in correct order.
     *
     * @class Server
     * @constructor
     */
    constructor () {
        // create an ExpressJS application
        this.app = express();

        // configure logging
        this.logging();

        // configure the application
        this.config();

        // load the routes for the application
        this.routes();

        // add an API for the application
        this.api();
    }

    /**
     * Configuration function for setting up application logging.
     *
     * @class Server
     * @method logging
     */
    public logging () {
        this.app.use(morgan("combined", {stream: { write: message => logger.info(message) } }));
        logger.info("Successfully loaded logging");
    }

    /**
     * Configuration function for the application.
     *
     * Several things are going on inside this function:
     * 1. any file located in /public is available as a static resource.
     * 2. the views folder gets loaded and the PugJS engine is loaded to parse those files.
     * 3. morgan was installed as the logger middleware that lets us do more later - haven't done anything with it for now.
     * 4. when we eventually want to build our own API we'll need body-parser.
     * 5. cookie-parser for parsing... well, cookies.
     * 6. method-override for later use (e.g. error handling)
     * 7. catch any 404 errors and application exceptions
     *
     * @class Server
     * @method config
     */
    public config () {
        // add static paths
        this.app.use(express.static(path.join(__dirname, "public")));

        // configure the basedir in order to include static files in PugJS templates
        this.app.locals.basedir = path.join(__dirname, "/");

        // configure pug
        this.app.set("views", path.join(__dirname, "views"));
        this.app.set("view engine", "pug");

        // use the logger middleware
        // this.app.use(logger("dev"));

        // use JSON form parser middleware
        this.app.use(bodyParser.json());

        // use query string parser middleware
        this.app.use(bodyParser.urlencoded({
            extended: true
        }));

        // use cookie parser middleware
        // in production I'd use an actual secret in the OS ENV
        this.app.use(cookieParser("SECRET_WOULD_GO_HERE"));

        // use override middleware
        this.app.use(methodOverride());

        // catch any 404 error and forward it to the error handler
        this.app.use(function(err: any, req: express.Request, res: express.Response, next: express.NextFunction) {
            err.status = 404;
            next(err);
        });

        // handle errors
        this.app.use(errorHandler());

        logger.info("Successfully ran config() function");
    }

    public api () {
        // empty for now but I would build my own API to create a mobile app / native desktop apps
    }

    /**
     * Create router for the application.
     *
     * The router is important as it provides the link between the URL in the browser and the view function that gets rendered on the client side.
     * Each route, for the time being, will be loaded in manually until I learn how to do it programmatically.
     * Lastly the router needs to get used by the app.
     *
     * @class Server
     * @method config
     * @return void
     */
    private routes () {
        // create an instance of express router
        let router: express.Router;
        router = express.Router();

        // load the index route into the router
        IndexRoute.create(router);
        SearchRoute.create(router);
        ResultRoute.create(router);

        // use the router middleware
        this.app.use(router);

        logger.info("Successfully loaded router() function");
    }
}
