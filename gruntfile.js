module.exports = function (grunt) {
    "use strict";

    grunt.initConfig({
        copy: {
            build: {
                files: [
                    {
                        expand: true,
                        cwd: "./public",
                        src: ["**"],
                        dest: "./dist/public/assets"
                    },
                    {
                        expand: true,
                        cwd: "./src/views",
                        src: ["**"],
                        dest: "./dist/views"
                    }
                ]
            }
        },
        ts: {
            app: {
                files: [{
                    src: ["src/\*\*/\*.ts", "!src/.baseDir.ts"],
                    dest: "./dist"
                }],
                options: {
                    experimentalDecorators: true,
                    module: "commonjs",
                    target: "es6",
                    sourceMap: false
                }
            }
        },
        postcss: {
            options: {
                processors: [
                    require('postcss-easy-import')(),
                    require('postcss-custom-properties')(),
                    require('postcss-custom-selectors')(),
                    require('postcss-custom-media')(),
                    require('postcss-media-minmax')(),
                    require('postcss-color-function')(),
                    require('postcss-nesting')(),
                    require('pixrem')({
                        replace: true
                    }),
                    require('postcss-calc')({
                        preserve: false,
                        mediaQueries: true,
                        warnWhenCannotResolve: true
                    }),
                    require('autoprefixer')({
                        browsers: 'last 2 versions'
                    }),
                    require('cssnano')()
                ]
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: "./src/css",
                    src: [
                        "*.css",
                        "!_*.css"
                    ],
                    dest: "./dist/css"
                }]
            }
        },
        watch: {
            ts: {
                files: ["src/\*\*/\*.ts"],
                tasks: ["ts"]
            },
            views: {
                files: ["src/views/**/*.pug"],
                tasks: ["copy"]
            },
            postcss: {
                files: ["src/css/**/*.css"],
                tasks: ["postcss"]
            }
        }
    });

    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-postcss");
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-ts");

    grunt.registerTask("default", [
        "copy",
        "postcss",
        "ts"
    ]);
}
