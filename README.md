# Marvel Comics Explorer

Application demo that highlights typescript and postcss and aims to at least show where I would take the project in future iterations.

## Notes

I have not yet hosted the product and it does not work (at least in the sense it has no features). The application and HTTP server works and it builds successfully using grunt - but I didn't get much past that.

## Live version of the demo

http://ec2-54-153-36-128.us-west-1.compute.amazonaws.com:8080

Changes will be added here as I develop the application. For now at least it's accessible and online. :)

## Run it locally

### Installing dependencies
In order to run the application locally you'll first need to install dependencies from the app folder:

```cd ./marvel-comics-explorer & npm install```

### Building .ts files
Run grunt to make sure that the .ts files compile:

```npm run grunt```

### Running tests
Note that no tests exist yet...

You should run the tests to make sure it works on your machine and that there have not been any regressions:

```npm run tests```

### Running the application
You're now ready to run the application. You can do this by:

```npm run dev```

Note that the above uses `cross-env` to ensure compatibility between systems, but I have not got it to work on Windows yet.

Alternatively if you just want a version that doesn't watch for changes:

```npm run start```

## Goals of the project

### Well-structured and cleanly coded

status: met

I was originally going to use CoffeeScript to write this but at the last second decided to use TypeScript. I find it more pleasant to have the typing and Java-like variable, declarative-like writing that you don't normally get with JavaScript.

Each major route has its own .ts definition, a .css file for styles, a .pug file for templating, and would eventually have a reference to all the data interfaces it needs.

### Robust and tested

status: partially met

Having not had much experience with unit-testing I at least wanted to set some groundwork for writing tests and getting the application into a solid state. For this I added Mocha and Chai as dependencies and would probably use Enzyme as well.

Each data model uses a TypeScript interface - in the future this would pull data from the Ajax response (from the Marvel API). These immutable objects should not change. This makes the application more robust and follows standard conventions - rather than mutating objects.

Application state stays separate from the actual DOM because I haven't really used any DOM manipulation at all. I just render templates with the data I get back.

In the future if I had actual Ajax calls working then I'd have .pug templates specifically for this purpose, instead of modifying existing DOM elements.

### Be intuitive and easy to use

status: unmet-ish

I wanted the interface to be simple and not require any instruction to use. It should be intuitive and the user should feel like they have control.
Any input should generate an expected reaction.

The future goal would be to have a right-arrow below the search box that indicates the user can click it. Alternatively some flavour text would let them know that they can press enter to begin. The arrow would morph into a spinner while the browser waits for a response.

### Use some kind of Ajax

status: unmet

I have not met this goal but I would love for the application to pre-fetch result pages (and possibly even pre-render the top hit) to make things faster. Not exactly sure how I would implement this but it would definitely be cool.

### Coding concerns be separate

status: partially met

Much like MVC I wanted each core function of the application to be separate. I compartmentalized all the views, application source, and (in the future) data models from each other.

Furthermore I want an infrastructure that allows use of API calls to the back-end so the user can run live queries. I have not yet built this, though...

### Preprocessing and templating

status: met

Rather than just use HTML for templating I decided to go with PugJS (see pugjs.org for more information). I've always loved Jade and it's powerful if used in the right way. Similarly I think that cssnext (see cssnext.io for more information) is an appropriate - but not so crazy - use of CSS and makes the syntax more pleasurable without needing stuff like LESS or SCSS or Stylus.

It also just makes sense to follow standards development - the other reason I chose postcss-cssnext.

## Closing words

The end result (given less strict time constraints) would be a functional application, better modularized CSS and better use of individual templating elements. Alongside this I would implement immutable data models, Ajax prefetching and related-object-exploration.

I enjoyed working on this project despite failing to actually deliver a working product - but I feel as though I met the brief in every other way.
